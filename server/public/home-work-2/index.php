<?php
$name = 'Vitalii';
$age = 20;
$pi = pi();
$namesArrayNumberOne = array('alex', 'vova', 'tolya');
$namesArrayNumberTwo = array('kostya', 'olya');
$mila = 'mila';
$namesArrayNumberThree = array('gosha', $mila);
$jointArrayNumberOneAndTwo = array($namesArrayNumberOne, $namesArrayNumberTwo);
$jointArrayNumberTwoAndThree = array($namesArrayNumberTwo, $namesArrayNumberThree);
$resultArrayNumberOne = array($namesArrayNumberOne, $jointArrayNumberTwoAndThree);
$resultArrayNumberTwo = array($namesArrayNumberOne, $namesArrayNumberTwo, $namesArrayNumberThree);
?>
<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Home work #2</title>
</head>
<body>
<?php
echo "<br>\r\n My name is $name";
echo "<br>\r\n Im $age year old";
echo "<br>\r\n This is number pi: $pi";
echo "<br>\r\n This is array '['alex', 'vova', 'tolya']':";
foreach ($namesArrayNumberOne as $name) {
  echo "<br>\r\n" . $name;
}
echo "<br>\r\n This is array '[‘alex’, ‘vova’, ‘tolya’, [‘kostya’, ‘olya’]]':";
print_r($jointArrayNumberOneAndTwo);
echo "<br>\r\n This is array '[‘alex’, ‘vova’, ‘tolya’, [‘kostya’, ‘olya’, [‘gosha’, mila]]]':";
print_r($resultArrayNumberOne);
echo "<br>\r\n This is array '[[‘alex’, ‘vova’, ‘tolya’], [‘kostya’, ‘olya’], [‘gosha’, mila]]':";
print_r($resultArrayNumberTwo);
?>
</body>
</html>
