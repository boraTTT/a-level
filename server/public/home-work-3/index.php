<?php

echo "<pre>";
echo "*";
echo "</pre>";

echo "1. ";
echo 42 < 55 ? '55 more' : '42 more';

echo "<pre>";
echo "2. ";
echo rand(10, 50) > rand(10, 50) ? 'True' : 'False';
echo "</pre>";

$firstName = 'Vitalii';
$middleName = 'Vasilevich';
$lastName = 'Chernenko';

$fullName = "$lastName $firstName[0].$middleName[0].";

echo "<pre>";
echo "3. " . $fullName;
echo "</pre>";

$randomNumber = rand(1, 99999);

echo "<pre>";
echo "4. In number $randomNumber: " . substr_count((string)$randomNumber, '7') . " numbers '7'";
echo "</pre>";

$a = 3;

echo "<pre>";
echo "5.a) $a";
echo "</pre>";

$a = 10;
$b = 2;

echo "<pre>";
echo "5.b) $a + $b = " . ($a + $b);
echo "</pre>";
echo "<pre>";
echo "$a - $b = " . ($a - $b);
echo "</pre>";
echo "<pre>";
echo "$a * $b = " . ($a * $b);
echo "</pre>";
echo "<pre>";
echo "$a / $b = " . ($a / $b);
echo "</pre>";

$c = 15;
$d = 2;

$result = $c + $d;
echo "<pre>";
echo "5.c) $c + $d = $result";
echo "</pre>";

$a = 10;
$b = 2;
$c = 5;

echo "<pre>";
echo "5.d) $a + $b + $c = " . ($a + $b +$c);
echo "</pre>";

$a = 17;
$b = 10;
$c = $a - $b;
$d = 8;

echo "<pre>";
echo "5.e) d = $d";
echo "</pre>";

$result = $c + $d;

echo "<pre>";
echo "6. $c + $d = $result";
echo "</pre>";

$text = 'Hello World!';

echo "<pre>";
echo "7.a) $text";
echo "</pre>";

$text1 = 'Hello ';
$text2 = 'World!';

echo "<pre>";
echo "7.b) " . ($text1 .= $text2);
echo "</pre>";

$secondPerHour = 60 * 60;
$secondPerDay = $secondPerHour * 24;
$secondPerWeek = $secondPerDay * 7;
$secondPerMonth = $secondPerDay * 30;

echo "<pre>";
echo "8.c) In one hour $secondPerHour seconds";
echo "</pre>";
echo "<pre>";
echo "In one day $secondPerDay seconds";
echo "</pre>";
echo "<pre>";
echo "In one week $secondPerWeek seconds";
echo "</pre>";
echo "<pre>";
echo "In one month $secondPerMonth seconds";
echo "</pre>";

$var = 1;
$var += 12;
$var -= 14;
$var *= 5;
$var /= 7;
$var %= 1;

echo "<pre>";
echo "8. $var";
echo "</pre>";

echo "<pre>";
echo "**";
echo "</pre>";

$hour = date("H");
$minutes = date("i");
$seconds = date("s");


echo "<pre>";
echo "1. $hour:$minutes:$seconds";
echo "</pre>";

$text = 'Я';
$text .= ' хочу';
$text .= ' знать';
$text .= ' PHP!';

echo "<pre>";
echo "2. $text";
echo "</pre>";

$foo = 'bar';
$bar = 10;

echo "<pre>";
echo "3. " . $$foo;
echo "</pre>";

$a = 2;
$b = 4;

echo "<pre>";
echo '4. $a++ + $b = ' . ($a++ + $b); //2 + 4 = 6
echo "</pre>";
echo "<pre>";
echo '$a + ++$b = ' . ($a + ++$b); //3 + 5 = 8
echo "</pre>";
echo "<pre>";
echo '++$a + $b++ = ' . (++$a + $b++); //4 + 5 = 9
echo "</pre>";

$a = "text";

echo "<pre>";
echo '5. isset($a): ';
echo  isset($a);
echo "</pre>";
echo "<pre>";
echo 'gettype($a): ' . gettype($a);
echo "</pre>";
echo "<pre>";
echo 'is_null($a): ';
echo is_null($a) === null ? 'its null' : 'its not null';
echo "</pre>";
echo "<pre>";
echo 'empty($a): ';
echo empty($a) ? 'variable is empty' : 'variable is not empty';
echo "</pre>";
echo "<pre>";
echo 'is_integer($a): ';
echo is_integer($a) ? 'variable is integer' : 'variable is not integer';
echo "</pre>";
echo "<pre>";
echo 'is_double($a): ';
echo is_double($a) ? 'variable is double' : 'variable is not double';
echo "</pre>";
echo "<pre>";
echo 'is_string($a): ';
echo is_string($a) ? 'variable is string' : 'variable is not string';
echo "</pre>";
echo "<pre>";
echo 'is_numeric($a): ';
echo is_numeric($a) ? 'variable is numeric' : 'variable is not numeric';
echo "</pre>";
echo "<pre>";
echo 'is_bool($a): ';
echo is_bool($a) ? 'variable is boolean' : 'variable is not boolean';
echo "</pre>";
echo "<pre>";
echo 'is_scalar($a): ';
echo is_scalar($a) ? 'variable is scalar' : 'variable is not scalar';
echo "</pre>";
echo "<pre>";
echo 'is_array($a): ';
echo is_array($a) ? 'variable is array' : 'variable is not array';
echo "</pre>";
echo "<pre>";
echo 'is_object($a): ';
echo is_object($a) ? 'variable is object' : 'variable is not object';
echo "</pre>";

echo "<pre>";
echo "***";
echo "</pre>";

$number1 = 3;
$number2 = 5;

$result1 = $number1 + $number2;
$result2 = $number1 * $number2;
$result3 = ($number1 * $number1) + ($number2 * $number2);

echo "<pre>";
echo "1. 3 + 5 = " . $result1;
echo  "</pre>";
echo "<pre>";
echo "3 * 5 = " . $result2;
echo  "</pre>";
echo "<pre>";
echo "2. 3^2 + 5^2 = " . $result3;
echo  "</pre>";

$x = 5;
$y = 7;
$z = 12;

$result1 = ($x + $y + $z)/3;
$result2 = ($x + 1) - 2 * ($z - (2 * $x) + $y);

echo "<pre>";
echo "3. (5 + 7 + 12)/3 = " . $result1;
echo  "</pre>";
echo "<pre>";
echo "4. (x+1)−2(z−2x+y) = " . $result2;
echo  "</pre>";

$num = 11;

echo "<pre>";
echo "5. 11 % 3 = " . ($num % 3);
echo  "</pre>";
echo "<pre>";
echo "11 % 5 = " . ($num % 5);
echo  "</pre>";
echo "<pre>";
echo "11 + 30% = " . ($num + ($num * 0.3));
echo  "</pre>";
echo "<pre>";
echo "11 + 120% = " . ($num + ($num * 1.2));
echo  "</pre>";

$num1 = 7;
$num2 = 13;
$num3 = 348;

echo "<pre>";
echo "6. (7 + 7 * 0.4) + (13 + 13 * 0.84) = " . (($num1 * 0.4) + ($num2 * 0.84));
echo  "</pre>";
echo "<pre>";
echo "3 + 4 + 8 = " . (array_sum(str_split($num3)));
echo  "</pre>";

$num = 572;
$num2 = strval($num);
$num2[1] = 0;

echo "<pre>";
echo "7. 572 => 502: " . ($num2);
echo  "</pre>";
echo "<pre>";
echo "502 => 205: " . (strrev($num2));
echo  "</pre>";

$num = rand(1, 999);

echo "<pre>";
echo "8. Number: $num";
echo  "</pre>";
echo "<pre>";
echo $num % 2 === 0 ? 'Even' : 'Odd';
echo  "</pre>";